unreleased
----------
- reexport syscalls dependency

0.1.4
-----
- impl RuleSet for &RuleSet
- Remove thiserror dependency
- Update libseccomp and syscalls dependencies

0.1.3 and prior
----
Initial extrasafe release
