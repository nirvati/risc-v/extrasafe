#![deny(non_ascii_idents)]
#![deny(unsafe_code)]
#![deny(unused_results)]
#![allow(clippy::new_without_default)]
// Denied in CI
//#![warn(missing_docs)]
#![warn(trivial_casts, trivial_numeric_casts)]

//! extrasafe is a library that makes it easy to improve your program's security by selectively
//! allowing the syscalls it can perform via the Linux kernel's seccomp facilities.
//!
//! See the [`SafetyContext`] struct's documentation and the tests/ and examples/ directories for
//! more information on how to use it.

#[cfg(any(feature = "aarch64", target_arch = "aarch64"))]
pub mod aarch64;
#[cfg(any(feature = "x86_64", target_arch = "x86_64"))]
pub mod x86_64;
#[cfg(any(feature = "riscv64", target_arch = "riscv64"))]
pub mod riscv64;

#[cfg(target_arch = "x86_64")]
pub use x86_64::*;

#[cfg(target_arch = "aarch64")]
pub use aarch64::*;

#[cfg(target_arch = "riscv64")]
pub use riscv64::*;

pub mod builtins;

#[cfg(not(any(target_arch = "x86_64", target_arch = "aarch64", target_arch = "riscv64")))]
compile_error!("Extrasafe currently only supports the x86_64, aarch64 and riscv64 architectures. You will likely see other errors about Sysno enum variants not existing; this is why.");
